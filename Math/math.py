import random


def miller_rabin(n, k=30):
    if n <= 3:
        return n == 2 or n == 3
    neg_one = n - 1

    s, d = 0, neg_one

    while d % 2 == 0:
        s, d = s+1, d>>1

    for i in range(k):
        a = random.randrange(2, neg_one)
        x = pow(a, d, n)
        if x in (1, neg_one):
            continue
        for r in range(1, s):
            x = x ** 2 % n
            if x == 1:
                return False
            if x == neg_one:
                break
        else:
            return False
    return True


def get_random_prime(N=10**20):
    p = 1
    while not miller_rabin(p):
        p = random.randrange(N)
    return p


def extended_euclides(modulus, value):
    x, lastx = 0, 1
    a, b = modulus, value

    while b:
        a, q, b = b, a // b, a % b
        x, lastx = lastx - q * x, x
    result = (1 - lastx * modulus) // value

    if result < 0:
        result += modulus

    return result
