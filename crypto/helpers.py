import base64


def base64_encode(value):
    encoded_value = bytes(value, 'UTF-8')
    encoded_value = base64.encodebytes(encoded_value)
    return encoded_value


def base64_decode(value):
    decoded_value = base64.decodebytes(value)
    decoded_value = str(decoded_value, 'UTF-8')
    return decoded_value
