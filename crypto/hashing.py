BLOCK_SIZE = 16


def xor_hash(value):
    print('\n**** Generating XOR Hash ****')
    value = value.encode('UTF-8')
    value_size = len(value)
    padding = value_size % BLOCK_SIZE
    padding = 0 if padding == 0 else BLOCK_SIZE - padding
    value_size += padding

    if padding != 0:
        value += b'\x00' * padding

    result = bytearray(bytes(BLOCK_SIZE))

    for start in range(0, value_size, BLOCK_SIZE):
        block = value[start:(start + BLOCK_SIZE)]

        for i in range(0, BLOCK_SIZE):
            result[i] ^= block[i]

    return ''.join(map(chr, result))
