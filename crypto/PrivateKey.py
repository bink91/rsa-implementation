from math import log
from binascii import hexlify, unhexlify
from .hashing import xor_hash


class PrivateKey:
    def __init__(self, n, d):
        self.n = n
        self.d = d

    def decrypt(self, message):
        print('\n**** Private Key Decryption ****')
        block_size = int(log(self.n, 256)) + 1
        output_format = '%%0%dx' % (block_size * 2)

        result = []

        for start in range(0, len(message), block_size):
            block = message[start:start + block_size]
            block = int(hexlify(block), 16)
            block = pow(block, self.d, self.n)
            block = unhexlify((output_format % block).encode('UTF-8'))

            result.append(block)

        return b''.join(result).rstrip(b'\x00').decode('UTF-8')

    def encrypt(self, message):
        print('\n**** Private Key Encryption ****')
        message = message.encode('UTF-8')
        block_size = int(log(self.n, 256))
        output_format = '%%0%dx' % ((block_size + 1) * 2)

        result = []

        for start in range(0, len(message), block_size):
            block = message[start:start + block_size]  # Get one block of message
            block += b'\x00' * (block_size - len(block))  # Fill with zeros if needed
            block = int(hexlify(block), 16)  # Convert to integer (base 16)
            block = pow(block, self.d, self.n)  # Encrypt block
            block = unhexlify((output_format % block).encode('UTF-8'))  # Convert to binary

            result.append(block)  # Add block to result

        return b''.join(result)  # Join and return all encrypted blocks

    def sign(self, message):
        print('\n**** Digital Sign Generation ****')
        message_hash = xor_hash(message)
        print('Message Hash: {0}'.format(message_hash))

        return self.encrypt(message_hash)
