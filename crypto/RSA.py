import json
import os.path

from .helpers import base64_encode, base64_decode
from .KeyGen import KeyGen
from .PublicKey import PublicKey
from .PrivateKey import PrivateKey


class RSA:
    private_key = None
    public_key = None

    def generate_keys(self):
        key_generator = KeyGen()
        key_generator.generate_keys()

        self.public_key = key_generator.public_key
        self.private_key = key_generator.private_key

    def export_public_key(self, path):
        if self.public_key is None:
            print('Cannot export. Public key is empty.')
            return

        with open(path, 'wb') as file:
            public_key = json.dumps({'e': self.public_key.e, 'n': self.public_key.n})
            print('\n-----Public Key-----\n{key}'.format(key=public_key))
            public_key = base64_encode(public_key)
            file.write(public_key)
            file.close()

    def export_private_key(self, path):
        if self.private_key is None:
            print('Cannot export. Private key is empty.')
            return

        with open(path, 'wb') as file:
            private_key = json.dumps({'d': self.private_key.d, 'n': self.private_key.n})
            print('\n-----Private Key-----\n{key}\n'.format(key=private_key))
            private_key = base64_encode(private_key)
            file.write(private_key)
            file.close()

    def import_public_key(self, path):
        if not os.path.isfile(path):
            print('Cannot import public key. Files doesn\'t exists.')
            return

        with open(path, 'rb') as file:
            key_data = file.read()
            key_data = base64_decode(key_data)
            key_data = json.loads(key_data)

            self.public_key = PublicKey(key_data['n'], key_data['e'])

            file.close()

        return self.public_key

    def import_private_key(self, path):
        if not os.path.isfile(path):
            print('Cannot import private key. Files doesn\'t exists.')
            return

        with open(path, 'rb') as file:
            key_data = file.read()
            key_data = base64_decode(key_data)
            key_data = json.loads(key_data)

            self.private_key = PrivateKey(key_data['n'], key_data['d'])

            file.close()

        return self.private_key
