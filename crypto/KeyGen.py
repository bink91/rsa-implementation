from Math import get_random_prime, extended_euclides
from random import randrange
from math import gcd
from .PublicKey import PublicKey
from .PrivateKey import PrivateKey


class KeyGen:
    private_key = None
    public_key = None

    def generate_keys(self):
        print('\n**** Generating Keys ****')

        p = get_random_prime()
        q = get_random_prime()

        while p == q:
            q = get_random_prime()

        print('p = {p}\nq = {q}\n'.format(p=p, q=q))

        n = p * q
        print('n = {n}'.format(n=n))

        phi = (p - 1) * (q - 1)
        print(u'\u03D5'' = {phi}'.format(phi=phi))

        while True:
            d = randrange(phi)
            if gcd(d, phi) == 1:
                break

        e = extended_euclides(phi, d)
        print("e = {e}".format(e=e))

        self.private_key = PrivateKey(n, d)
        self.public_key = PublicKey(n, e)
