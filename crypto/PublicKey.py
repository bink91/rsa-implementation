from math import log
from binascii import hexlify, unhexlify


class PublicKey:
    def __init__(self, n, e):
        self.n = n
        self.e = e

    def encrypt(self, message):
        print('\n**** Public Key Encryption ****')
        message = message.encode('UTF-8')
        block_size = int(log(self.n, 256))
        output_format = '%%0%dx' % ((block_size + 1) * 2)

        result = []

        for start in range(0, len(message), block_size):
            block = message[start:start + block_size]  # Get one block of message
            block += b'\x00' * (block_size - len(block))  # Fill with zeros if needed
            block = int(hexlify(block), 16)  # Convert to integer (base 16)
            block = pow(block, self.e, self.n)  # Encrypt block
            block = unhexlify((output_format % block).encode('UTF-8'))  # Convert to binary

            result.append(block)  # Add block to result

        return b''.join(result)  # Join and return all encrypted blocks

    def decrypt(self, message):
        print('\n**** Public Key Decryption ****')
        block_size = int(log(self.n, 256)) + 1
        output_format = '%%0%dx' % (block_size * 2)

        result = []

        for start in range(0, len(message), block_size):
            block = message[start:start + block_size]
            block = int(hexlify(block), 16)
            block = pow(block, self.e, self.n)
            block = unhexlify((output_format % block).encode('UTF-8'))

            result.append(block)

        return b''.join(result).rstrip(b'\x00').decode('UTF-8')
