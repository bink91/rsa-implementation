"""
    Author: Rafał Zbytniewski
    E-mail: rafal.zbytniewski@gmail.com
    Python Version: Python 3.5.2+
"""

from crypto import RSA, xor_hash
import sys
import os.path

rsa = RSA()


def keygen():
    rsa.generate_keys()
    rsa.export_public_key('keys/public')
    rsa.export_private_key('keys/private')


def encrypt(source_file_path, public_key_path, output_file_path):
    if os.path.isfile(source_file_path) and os.path.isfile(public_key_path):
        with open(source_file_path, 'r') as source_file:
            source = source_file.read()
            encrypted = rsa.import_public_key(public_key_path).encrypt(source)

            with open(output_file_path, 'wb') as output_file:
                output_file.write(encrypted)
                output_file.close()

            source_file.close()
    else:
        print('Some paths are wrong.')


def decrypt(source_file_path, private_key_path, output_file_path):
    if os.path.isfile(source_file_path) and os.path.isfile(private_key_path):
        with open(source_file_path, 'rb') as source_file:
            source = source_file.read()
            decrypted = rsa.import_private_key(private_key_path).decrypt(source)
            decrypted = decrypted.encode('UTF-8')

            with open(output_file_path, 'wb') as output_file:
                output_file.write(decrypted)
                output_file.close()

            source_file.close()
    else:
        print('Some paths are wrong.')


def sign(source_file_path, private_key_path, output_file_path):
    if os.path.isfile(source_file_path) and os.path.isfile(private_key_path):
        with open(source_file_path, 'r') as source_file:
            source = source_file.read()
            signature = rsa.import_private_key(private_key_path).sign(source)

            with open(output_file_path, 'wb') as output_file:
                output_file.write(signature)
                output_file.close()

            source_file.close()
    else:
        print('Some paths are wrong.')


def verify(source_file_path, public_key_path, signature_file_path):
    if os.path.isfile(source_file_path) and os.path.isfile(public_key_path) and os.path.isfile(signature_file_path):
        print('\n**** Digital Sign Verification ****')

        with open(source_file_path, 'r') as source_file:
            source = source_file.read()
            source_hash = xor_hash(source)
            print('Source Hash = {0} (len: {1})'.format(source_hash, len(source_hash)))

            with open(signature_file_path, 'rb') as signature_file:
                signature = signature_file.read()
                signature = rsa.import_public_key(public_key_path).decrypt(signature)

                print('Signature = {0} (len: {1})'.format(signature, len(signature)))

                if source_hash == signature:
                    print('Digital Sign Verified!')
                else:
                    print('Verification Failed!')

                signature_file.close()

            source_file.close()
    else:
        print('Some paths are wrong.')


if __name__ == '__main__':
    print('\n\n\t\t-----RSA Sandbox v.0.1-----\n\t\tBy Rafał Zbytniewski rafal.zbytniewski@gmail.com')

    print("""
        Usage:

        keygen
        encrypt [-f <source file>] -k <public key> -o <output file>
        decrypt [-f <source file>] -k <private key> -o <output file>
        sign -f <source file> -k <private key> -o <output file>
        verify -f <source file> -s <signature> -k <public key>
    """)

    # Examples of use:
    #
    # keygen()
    # encrypt('resources/text_file_1.txt', 'keys/public', 'encrypted_file')
    # decrypt('encrypted_file', 'keys/private', 'decrypted_file')
    # sign('resources/text_file_1.txt', 'keys/private', 'my_signature')
    # verify('resources/text_file_1.txt', 'keys/public', 'my_signature')

    if len(sys.argv) > 1:
        if sys.argv[1] == 'keygen':
            keygen()
        elif sys.argv[1] == 'encrypt':
            if sys.argv[2] == '-f' and sys.argv[3] is not None and sys.argv[4] == '-k' and sys.argv[5] is not None and \
                            sys.argv[6] == '-o' and sys.argv[7] is not None:
                encrypt(sys.argv[3], sys.argv[5], sys.argv[7])
        elif sys.argv[1] == 'decrypt':
            if sys.argv[2] == '-f' and sys.argv[3] is not None and sys.argv[4] == '-k' and sys.argv[5] is not None and \
                        sys.argv[6] == '-o' and sys.argv[7] is not None:
                decrypt(sys.argv[3], sys.argv[5], sys.argv[7])
        else:
            print('Unknown command.')
